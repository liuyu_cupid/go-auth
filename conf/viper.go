package conf

import (
	"fmt"
	"github.com/spf13/viper"
)

func init() {
	viper.AddConfigPath("./conf")
	viper.AddConfigPath("$HOME/etc")
	viper.SetConfigName("go-auth")
	viper.SetConfigType("yaml")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println(err)
	}
}
