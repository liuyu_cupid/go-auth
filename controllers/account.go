package controllers

import (
	"encoding/json"
	"go-auth/models"
	"go-auth/token"
	"go-auth/tools"
	"time"

	beego "github.com/beego/beego/v2/server/web"
)

// AccountController Operations about Users
type AccountController struct {
	beego.Controller
}

type AccountDto struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Post @Title CreateAccount
// @Description create users
// @Param	body		body 	models.Account	true		"body for user content"
// @Success 200 {int} models.Account.Id
// @Failure 403 body is empty
// @router / [post]
func (a *AccountController) Post() {
	var account models.Account
	json.Unmarshal(a.Ctx.Input.RequestBody, &account)
	uid := models.AddAccount(account)
	a.Data["json"] = map[string]string{"uid": uid}
	a.ServeJSON()
}

// GetAll @Title GetAll
// @Description get all Accounts
// @Success 200 {object} models.Account
// @router / [get]
func (a *AccountController) GetAll() {
	accounts := models.GetAllAccounts()
	a.Data["json"] = ApiResponse{Code: success, Message: "", Data: accounts}
	a.ServeJSON()
}

// Get @Title Get
// @Description get account by id
// @Param	uid		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Account
// @Failure 403 :uid is empty
// @router /:uid [get]
func (a *AccountController) Get() {
	uid := a.GetString(":uid")
	if uid != "" {
		account, err := models.GetAccount(uid)
		if err != nil {
			a.Data["json"] = err.Error()
		} else {
			a.Data["json"] = account
		}
	}
	a.ServeJSON()
}

// Put @Title Update
// @Description update the user
// @Param	uid		path 	string	true		"The uid you want to update"
// @Param	body		body 	models.Account	true		"body for user content"
// @Success 200 {object} models.Account
// @Failure 403 :uid is not int
// @router /:aid [put]
func (a *AccountController) Put() {
	uid := a.GetString(":aid")
	if uid != "" {
		var account models.Account
		json.Unmarshal(a.Ctx.Input.RequestBody, &account)
		uu, err := models.UpdateAccount(uid, &account)
		if err != nil {
			a.Data["json"] = err.Error()
		} else {
			a.Data["json"] = uu
		}
	}
	a.ServeJSON()
}

// Delete @Title Delete
// @Description delete the user
// @Param	uid		path 	string	true		"The uid you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 uid is empty
// @router /:aid [delete]
func (a *AccountController) Delete() {
	uid := a.GetString(":aid")
	models.DeleteAccount(uid)
	a.Data["json"] = "delete success!"
	a.ServeJSON()
}

// Login @Title Login
// @Description Logs user into the system
// @Param	username		query 	string	true		"The username for login"
// @Param	password		query 	string	true		"The password for login"
// @Success 200 {string} login success
// @Failure 403 user not exist
// @router /login [post]
func (a *AccountController) Login() {
	var accountDto models.Account
	json.Unmarshal(a.Ctx.Input.RequestBody, &accountDto)
	if models.Login(accountDto.Username, accountDto.Password) {
		jwtToken, _ := token.GenToken(accountDto.Username)
		a.Data["json"] = ApiResponse{
			Code:    success,
			Message: "success",
			Data:    jwtToken,
		}
		tools.RedisClient.Set(accountDto.Username, jwtToken, time.Minute*30)
	} else {
		a.Data["json"] = ApiResponse{
			Code:    fail,
			Message: "fail",
			Data:    nil,
		}
	}
	a.ServeJSON()
}

// Logout @Title logout
// @Description Logs out current logged in user session
// @Success 200 {string} logout success
// @router /logout [get]
func (a *AccountController) Logout() {
	jwtToken := a.Ctx.Input.Header("Authorization")
	username, _ := token.ParseToken(jwtToken)
	tools.RedisClient.Del(username)
	a.Data["json"] = "logout success"
	a.ServeJSON()
}
