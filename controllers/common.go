package controllers

const (
	success = 200
	fail    = 500
)

// 通用的返回结构体
type ApiResponse struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

var FailResponse = &ApiResponse{
	Code:    fail,
	Message: "fail",
	Data:    nil,
}