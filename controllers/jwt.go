package controllers

import (
	"fmt"
	beego "github.com/beego/beego/v2/server/web"
	"github.com/spf13/viper"
	"go-auth/token"
	"go-auth/tools"
	"time"
)

type JwtController struct {
	beego.Controller
}

// @Title PublicKey
// @Description Get jwt public key
// @Success 200 {string} login success
// @Failure 500 serve fail
// @router /publicKey [get]
func (c *JwtController) PublicKey() {
	c.Data["json"] = &ApiResponse{
		Code:    success,
		Message: "success",
		Data:    viper.GetString("jwt.public-key"),
	}
	c.ServeJSON()
}

// @Title Refresh
// @Description Refresh jwt token
// @Success 200 {string} refresh token success
// @Failure 500 serve fail
// @router /refresh [get]
func (c *JwtController) Refresh() {
	oldToken := c.Ctx.Input.Header("Authorization")
	parseToken, err := token.ParseToken(oldToken)
	if err != nil {
		c.Data["json"] = FailResponse
		c.ServeJSON()
		return
	}
	newToken, err := token.GenToken(parseToken)
	// 新建token数据
	set := tools.RedisClient.Set(newToken, tools.RedisClient.Get(oldToken), time.Minute*30)
	fmt.Println(set)
	// 删除旧的
	tools.RedisClient.Del(oldToken)
	if err != nil {
		c.Data["json"] = FailResponse
		c.ServeJSON()
		return
	}
	c.Data["json"] = &ApiResponse{
		Code:    success,
		Message: "success",
		Data:    newToken,
	}
	c.ServeJSON()
}

// @Title IsLogin
// @Description Verify that the token is valid
// @Success 200 {bool} login success
// @Failure 500 serve fail
// @router /isLogin [get]
func (c *JwtController) IsLogin() {
	jwtToken := c.GetString("token")
	exists := tools.RedisClient.Exists(jwtToken)
	c.Data["json"] = &ApiResponse{
		Code:    success,
		Message: "success",
		Data:    exists.Val() != 0,
	}
	c.ServeJSON()
}
