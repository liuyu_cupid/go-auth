package filter

import (
	"encoding/json"
	"fmt"
	"github.com/beego/beego/v2/server/web/context"
	"go-auth/controllers"
	"go-auth/token"
	"go-auth/tools"
)

// 检查是否登录过滤器
var LoginFilter = func(ctx *context.Context) {
	jwtToken := ctx.Input.Header("Authorization")
	username, e := token.ParseToken(jwtToken)
	if e != nil {
		panic(e)
	}
	exists := tools.RedisClient.Exists(username)
	fmt.Println(ctx.Request.RequestURI)
	if exists.Val() == 0 {
		marshal, _ := json.Marshal(controllers.ApiResponse{
			Code:    500,
			Message: "未登录",
			Data:    nil,
		})
		ctx.WriteString(string(marshal))
	}
}
