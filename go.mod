module go-auth

go 1.15

require github.com/beego/beego/v2 v2.0.1

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis v6.14.2+incompatible
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
)
