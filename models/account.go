package models

import (
	"errors"
	"strconv"
	"time"
)

var (
	UserList map[string]*Account
)

func init() {
	UserList = make(map[string]*Account)
	u1 := Account{"1", "admin", "admin", Profile{"male", 20, "Singapore", "admin@gmail.com"}}
	UserList[u1.Id] = &u1
	u2 := Account{"2", "root", "123", Profile{"female", 18, "Singapore", "root@gmail.com"}}
	UserList[u2.Id] = &u2
}

type Account struct {
	Id       string
	Username string
	Password string
	Profile  Profile
}

type Profile struct {
	Gender  string
	Age     int
	Address string
	Email   string
}

// 用户登录设备信息
type UserDevice struct {
	UserAgent string
}

func AddAccount(a Account) string {
	a.Id = "user_" + strconv.FormatInt(time.Now().UnixNano(), 10)
	UserList[a.Id] = &a
	return a.Id
}

func GetAccount(uid string) (u *Account, err error) {
	if u, ok := UserList[uid]; ok {
		return u, nil
	}
	return nil, errors.New("Account not exists")
}

func GetAllAccounts() []*Account {
	var userList []*Account
	for _, value := range UserList {
		userList = append(userList, value)
	}
	return userList
}

func UpdateAccount(uid string, uu *Account) (a *Account, err error) {
	if u, ok := UserList[uid]; ok {
		if uu.Username != "" {
			u.Username = uu.Username
		}
		if uu.Password != "" {
			u.Password = uu.Password
		}
		if uu.Profile.Age != 0 {
			u.Profile.Age = uu.Profile.Age
		}
		if uu.Profile.Address != "" {
			u.Profile.Address = uu.Profile.Address
		}
		if uu.Profile.Gender != "" {
			u.Profile.Gender = uu.Profile.Gender
		}
		if uu.Profile.Email != "" {
			u.Profile.Email = uu.Profile.Email
		}
		return u, nil
	}
	return nil, errors.New("Account Not Exist")
}

func Login(username, password string) bool {
	for _, u := range UserList {
		if u.Username == username && u.Password == password {
			return true
		}
	}
	return false
}

func DeleteAccount(uid string) {
	delete(UserList, uid)
}
