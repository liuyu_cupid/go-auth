package routers

import (
	beego "github.com/beego/beego/v2/server/web"
	"github.com/beego/beego/v2/server/web/context/param"
)

func init() {

    beego.GlobalControllerRouter["go-auth/controllers:AccountController"] = append(beego.GlobalControllerRouter["go-auth/controllers:AccountController"],
        beego.ControllerComments{
            Method: "Post",
            Router: "/",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["go-auth/controllers:AccountController"] = append(beego.GlobalControllerRouter["go-auth/controllers:AccountController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: "/",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["go-auth/controllers:AccountController"] = append(beego.GlobalControllerRouter["go-auth/controllers:AccountController"],
        beego.ControllerComments{
            Method: "Put",
            Router: "/:aid",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["go-auth/controllers:AccountController"] = append(beego.GlobalControllerRouter["go-auth/controllers:AccountController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/:aid",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["go-auth/controllers:AccountController"] = append(beego.GlobalControllerRouter["go-auth/controllers:AccountController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/:uid",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["go-auth/controllers:AccountController"] = append(beego.GlobalControllerRouter["go-auth/controllers:AccountController"],
        beego.ControllerComments{
            Method: "Login",
            Router: "/login",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["go-auth/controllers:AccountController"] = append(beego.GlobalControllerRouter["go-auth/controllers:AccountController"],
        beego.ControllerComments{
            Method: "Logout",
            Router: "/logout",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["go-auth/controllers:JwtController"] = append(beego.GlobalControllerRouter["go-auth/controllers:JwtController"],
        beego.ControllerComments{
            Method: "IsLogin",
            Router: "/isLogin",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["go-auth/controllers:JwtController"] = append(beego.GlobalControllerRouter["go-auth/controllers:JwtController"],
        beego.ControllerComments{
            Method: "PublicKey",
            Router: "/publicKey",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["go-auth/controllers:JwtController"] = append(beego.GlobalControllerRouter["go-auth/controllers:JwtController"],
        beego.ControllerComments{
            Method: "Refresh",
            Router: "/refresh",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
