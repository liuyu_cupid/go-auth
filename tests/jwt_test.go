package test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	_ "go-auth/conf"
	token2 "go-auth/token"
	"testing"
)

func TestGenToken(t *testing.T) {
	account := "liuyu"
	token, err := token2.GenToken(account)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(token)
	parseToken, _ := token2.ParseToken(token)

	assert.Equal(t, account, parseToken)
}
