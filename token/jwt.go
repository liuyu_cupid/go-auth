package token

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"
	"time"
)

var hmacSampleSecret = []byte(viper.GetString("jwt.secret"))

// 自定义Claim
type MyClaim struct {
	jwt.StandardClaims
	Username string `json:"username"`
}

// 生成token
func GenToken(username string) (string, error) {
	claims := &MyClaim{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(24 * 30 * time.Hour).Unix(), // 过期时间，必须设置
			Issuer:    "go-auth",                                  // 可不必设置，也可以填充用户名，
		},
		Username: username,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	secret := append(hmacSampleSecret, []byte(username)...)
	return token.SignedString(secret)
}

// 解析token返回账号
func ParseToken(tokenString string) (string, error) {
	// Parse takes the token string and a function for looking up the key. The latter is especially
	// useful if you use multiple keys for your application.  The standard is to use 'kid' in the
	// head of the token to identify which key to use, but the parsed token (head and claims) is provided
	// to the callback, providing flexibility.
	parseToken, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return hmacSampleSecret, nil
	})

	if claims, ok := parseToken.Claims.(jwt.MapClaims); ok && parseToken.Valid {
		return claims["username"].(string), nil
	} else {
		return "", err
	}
}
