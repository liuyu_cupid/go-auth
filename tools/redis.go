package tools

import (
	"github.com/go-redis/redis"
	"github.com/spf13/viper"
)

type redisTemplate struct {
	*redis.Client
}

var RedisClient redisTemplate

func init() {
	options := redis.Options{
		Addr:     viper.GetString("redis.host"),
		Password: viper.GetString("redis.password"),
		DB:       1,
	}

	client := redis.NewClient(&options)
	
	RedisClient = redisTemplate{
		client,
	}
}
